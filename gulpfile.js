/*copy src into cache*/
var gulp = require('gulp');

// takes in a callback so the engine knows when it'll be done
gulp.task('tmp', function(cb) {
    // do stuff -- async or otherwise
    cb(err); // if err is not null and not undefined, the run will stop, and note that it failed
});

// identifies a dependent task must be complete before this one begins
gulp.task('AngularAOT', ['one'], function() {
    // task 'one' is done now
});

gulp.task('default', ['one', 'two']);
//compile Angular into dist
//copy only webrowser compliant files into dist
