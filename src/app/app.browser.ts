import 'zone.js/dist/zone';
import 'reflect-metadata';

import { platformBrowser } from '@angular/platform-browser';
import { AppModuleNgFactory } from './app.ngfactory';

console.log('Bootstrapping dynamic version of application...');

const start = new Date();
platformBrowser().bootstrapModuleFactory(AppModuleNgFactory)
    .then(() => console.log(`Dynamic bootstrap complete in ${new Date().getTime() - start.getTime()}ms`))
    .catch((err)=>{console.log(err)})
