import { NgModule, Component }      from '@angular/core';
import { BrowserModule}             from '@angular/platform-browser';
import { ServerModule }             from '@angular/platform-server';
import {RouterModule}               from '@angular/router';
import {PreventDefaultHostBiding}    from './shared/directive';
//import routes                   from './routing/routes'

import {MonkModule, MonkOverviewState}             from './modules/monk'

@Component ({
    selector:'zmb-ssr',
    template:`
        <h2>THE MONK</h2>
        <router-outlet></router-outlet>
    `
})
export class AppComponent{

};

@NgModule({
    imports:      [
       BrowserModule.withServerTransition({appId:'zmbSSR'}),
       ServerModule,
       RouterModule.forRoot([
           MonkOverviewState,
           {   path:'',
               redirectTo:'/monk',
               pathMatch:'full'}]),
       MonkModule,
    ],
    declarations: [ AppComponent,PreventDefaultHostBiding ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }
