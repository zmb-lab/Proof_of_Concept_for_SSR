import {Component} from '@angular/core';

@Component({
    selector: 'monk-overview',
    template: `
    <h1>A monk sits in a temple</h1>
    <br>
    <a [routerLink]="['./detail'] ">Tell me more</a>
    <router-outlet></router-outlet>
    `
})
export class MonkOverviewComponent {

}
