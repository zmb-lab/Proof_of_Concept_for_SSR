import {NgModule} from '@angular/core';
import {MonkOverviewComponent} from './components/Overview/MonkOverview';
import {RouterModule,Route } from '@angular/router';
import { BrowserModule} from '@angular/platform-browser';
import { ServerModule } from '@angular/platform-server';
import {MonkDetailComponent} from './components/Detail/MonkDetail';

@NgModule({
    imports:[RouterModule,BrowserModule,ServerModule],
    declarations:[MonkDetailComponent, MonkOverviewComponent]
})
export class MonkModule {

}


const MonkDetailState:Route = {
    path:'detail',
    component: MonkDetailComponent
}

export const MonkOverviewState:Route = {
    path: 'monk',
    component: MonkOverviewComponent,
    children: [
        MonkDetailState
    ]
}
