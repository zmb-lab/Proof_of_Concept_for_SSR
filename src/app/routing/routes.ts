import {Routes,Route} from '@angular/router';
import {MonkOverviewState} from '../modules/monk';

const main:Route = {
    path:'',
    redirectTo:'/monk',
    pathMatch:'full'
}

const route404:Route = {
    path:'ass',
    redirectTo:'/monk'
}

 const routes: Routes = [
      MonkOverviewState,main,route404
    ]
 export default routes;
