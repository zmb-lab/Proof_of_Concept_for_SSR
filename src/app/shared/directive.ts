import { Directive, HostListener } from '@angular/core';



@Directive({ selector: 'a' })
export class PreventDefaultHostBiding {
    @HostListener("click")
        onClick(event:any) {
	    event.preventDefault();
        }
}
