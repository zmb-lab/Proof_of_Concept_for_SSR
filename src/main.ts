import 'zone.js/dist/zone-node';
import 'rxjs';
import 'reflect-metadata';
import * as express from 'express';
import { ngExpressEngine } from '@nguniversal/express-engine';
import routes from './app/routing/routes';
//AppModuleNgFactory is only available after the app is compiled

import {AppModuleNgFactory} from './app/app.ngfactory';
const server = express();

// Set the engine
server.engine('html', ngExpressEngine({
  bootstrap: AppModuleNgFactory // Give it a module to bootstrap(u should use ROOT Module)
}));

function ngApp(req:any, res:any) {
  res.render('./index', {
    req,
    res
    //preboot: false, // turn on if using preboot
    //baseUrl: '/',
    //requestUrl: req.originalUrl,
    //originUrl: `http://localhost:${ server.get('port')}`
  });
}

server.set('view engine', 'html');
server.set('views', 'dist/out');
server.use(express.static('dist/out'));

server.get('/', ngApp);
routes.forEach( route => {
    if(route.path){
        server.get(`/${route.path}`  , ngApp)
        server.get(`/${route.path}/*`, ngApp)
    }
});
server.listen('8000');
