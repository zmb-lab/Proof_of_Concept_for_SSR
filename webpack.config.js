const path = require('path');

module.exports = {
  entry: './src/app/app.browser.ts',
  resolve:{
    extensions: [".ts",".js"]
  },
  module:
  {
    rules:[
      { test:/.ts$/, use: 'ts-loader'}
    ]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist/out')
  }
};
